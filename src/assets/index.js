/*
1. fonst
2. imgs
3. icones
4. etc
*/

import LOCAL_CRIME from './img/local_crime.jpg';
import COMP_FORENSE from './img/computacao_forense.jpg';
import PER_2 from './img/per2.jpg';

import Foto2 from './img/Foto2.png';
import Foto3 from './img/Foto3.png';
import Foto4 from './img/Foto4.png';
import Foto5 from './img/Foto5.png';
import Foto6 from './img/Foto6.png';
import Foto7 from './img/Foto7.png';
import Foto8 from './img/Foto8.png';
import areaExterna from './img/AreaExterna.png';
import passarela from './img/Passarela1.png';
import patioCoberto from './img/PatioCoberto.png';
import fossa from './img/fossa.png';
import tabelaCrecheIII from './img/tabela-crecheiii.png';
import p2repouso from './img/p2repouso.png';
import p2repouso2 from './img/p2repouso2.png';
import tabelap2repouso from './img/tabela-p2repouso.png';
import p2creche3 from './img/p2creche3.png';
import p2solarium1 from './img/p2solarium1.png';
import p2tabelasolarium1 from './img/p2tabelasolarium1.png';
import p2tabelasolarium2 from './img/p2tabelasolarium2.png';
import p2solarium2 from './img/p2solarium2.png';
import adm_rec1 from './img/adm_rec1.png';
import adm_rec2 from './img/adm_rec2.png';
import adm_direx1 from './img/adm_direx1.png';
import adm_direx2 from './img/adm_direx2.png';
import adm_prof1 from './img/adm_prof1.png';
import adm_prof2 from './img/adm_prof2.png';
import adm_circ1 from './img/adm_circ1.png';
import adm_alm1 from './img/adm_alm1.png';
import adm_alm2 from './img/adm_alm2.png';
import adm_alm3 from './img/adm-almx-prateleiras.png';
import adm_alm4 from './img/adm_alm4.png';
import adm_ban1 from './img/adm_ban1.png';
import adm_ban2 from './img/adm_ban2.png';
import adm_ban3 from './img/adm-ban1-divisorias.png';
import ban2 from './img/ban2.png';
import creche2 from './img/creche2.png';
import creche21 from './img/creche21.png';
import p1repouso1 from './img/p1repouso1.png';
import p1repouso12 from './img/p1repouso12.png';
import p1creche2 from './img/p1creche2.png';
import p1creche2tabela from './img/p1creche2tabela.png';
import p1repouso2 from './img/p1repouso2.png';
import p1banheiro1 from './img/p1banheiro1.png';
import p1banheiro1tab from './img/p1banheiro1tab.png';
import p1solarium1 from './img/p1solarium1.png';
import p1solariumtab1 from './img/p1solariumtab1.png';
import p1creche1 from './img/p1creche1.png';
import p1creche1tab from './img/p1creche1tab.png';
import fraldario1 from './img/fraldario1.png';
import fraldario2 from './img/fraldario2.png';
import fraldario3 from './img/fraldario3.png';
import fraldario22 from './img/fraldario22.png';
import solarium2 from './img/solarium2.png';
import p1banheiro2 from './img/p1banheiro2.png';
import servarea1 from './img/servarea1.png';
import servarea2 from './img/servarea2.png';
import servarea3 from './img/servarea3.png';
import servdeposito1 from './img/servdeposito1.png';
import servdeposito2 from './img/servdeposito2.png';
import servdeposito3 from './img/servdeposito3.png';
import servcozinha1 from './img/servcozinha1.png';
import servcozinha2 from './img/servcozinha2.png';
import servcozinha3 from './img/servcozinha3.png';
import servper1 from './img/servper1.png';
import servper2 from './img/servper2.png';
import servdml1 from './img/servdml1.png';
import servdml2 from './img/serv_dml2.png';
import servvestiario2 from './img/servvestiario2.png';
import servestiario2 from './img/servestiario2.png';
import servestiario3 from './img/servestiario3.png';
import servvestf1 from './img/servvestf1.png';
import servvestf2 from './img/servvestf2.png';
import servvestf3 from './img/servvestf3.png';
import servlav1 from './img/servlav1.png';
import servlav2 from './img/servlav2.png';
import servlav3 from './img/servlav3.png';
import servroup1 from './img/servroup1.png';
import servrop2 from './img/servrop2.png';
import servlac1 from './img/servlac1.png';
import servlac2 from './img/servlac2.png';
import serv_reservatorio from './img/serv_reservatorio.png';
import p2preescola1 from './img/p2preescola1.png';
import p2preescola2 from './img/p2preescola2.png';
import p2tabelapreescola1 from './img/p2tabelapreescola1.png';
import p2tabelapreescola2 from './img/p2tabelapreescola2.png';
import tabelasanitariopne1 from './img/tabela-sanitariopne1.png';
import tabelasanitario1 from './img/tabela-sanitario1.png';
import sanitariopne2 from './img/sanitariopne2.png';
import sanitario1 from './img/sanitario1.png';
import sanitario2 from './img/sanitario2.png';
import divisoriasanitario1 from './img/sanitario1-divisoria.png';
import leituramultiuso from './img/leituramultiuso.png';
import tabelaleitura from './img/tabela-leitura.png';
import labinformatica from './img/labinformatica.png';
import tabelainformatica from './img/tabela-informatica.png';
import rack from './img/rack.png';
import tabelarack from './img/tabelarack.png';
import ciatel from './img/ciatel.png';
import tabelaciatel from './img/tabelaciatel.png';
import ciaele from './img/ciaele.png';
import tabelaciaele from './img/tabelaciaele.png';

export const Imgs = {
  LOCAL_CRIME,
  COMP_FORENSE,
  PER_2,
};

export const MolequaImgs = {
  Foto2,
  Foto3,
  Foto4,
  Foto5,
  Foto6,
  Foto7,
  Foto8,
  areaExterna,
  passarela,
  patioCoberto,
  fossa,
  tabelaCrecheIII,
  p2repouso,
  tabelap2repouso,
  p2repouso2,
  p2creche3,
  p2solarium1,
  p2tabelasolarium1,
  p2solarium2,
  p2tabelasolarium2,
  adm_rec1,
  adm_rec2,
  adm_direx1,
  adm_direx2,
  adm_prof1,
  adm_prof2,
  adm_circ1,
  adm_alm1,
  adm_alm2,
  adm_alm3,
  adm_alm4,
  adm_ban1,
  adm_ban2,
  adm_ban3,
  ban2,
  creche2,
  creche21,
  p1repouso1,
  p1repouso12,
  p1creche2,
  p1creche2tabela,
  p1repouso2,
  p1banheiro1,
  p1banheiro1tab,
  p1solarium1,
  p1solariumtab1,
  p1creche1,
  p1creche1tab,
  fraldario1,
  fraldario2,
  fraldario3,
  fraldario22,
  solarium2,
  p1banheiro2,
  servarea1,
  servarea2,
  servarea3,
  servdeposito1,
  servdeposito2,
  servdeposito3,
  servcozinha1,
  servcozinha2,
  servcozinha3,
  servper1,
  servper2,
  servdml1,
  servdml2,
  servvestiario2,
  servestiario2,
  servestiario3,
  servvestf1,
  servvestf2,
  servvestf3,
  servlav1,
  servlav2,
  servlav3,
  servroup1,
  servrop2,
  servlac1,
  servlac2,
  serv_reservatorio,
  p2preescola1,
  p2preescola2,
  p2tabelapreescola1,
  p2tabelapreescola2,
  tabelasanitariopne1,
  sanitariopne2,
  sanitario1,
  tabelasanitario1,
  divisoriasanitario1,
  sanitario2,
  leituramultiuso,
  tabelaleitura,
  labinformatica,
  tabelainformatica,
  rack,
  tabelarack,
  ciatel,
  tabelaciatel,
  ciaele,
  tabelaciaele,
};
